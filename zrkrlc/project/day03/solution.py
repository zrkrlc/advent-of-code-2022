from aocd import get_data
import functools as f

def rucksack_split(xs):
    return [xs[:len(xs)//2], xs[len(xs)//2:]]

def priority(c):
    return (ord(c) - 96) if 96 < ord(c) else ((ord(c) - 64) + 26)

def solution():
    raw_input = get_data(day=3, year=2022)
    input = raw_input.splitlines()

    # Part I
    rucksacks = map(rucksack_split, input)
    common_items = map(lambda bag: ''.join(set(bag[0]).intersection(bag[1])), rucksacks)
    priorities = map(priority, common_items)
    
    # Part II
    groups = [input[i:i+3] for i in range(0, len(input), 3)]
    badges = map(lambda group: ''.join(set(group[0]).intersection(group[1]).intersection(group[2])), groups)
    priorities_badges = map(priority, badges)

    return (sum(priorities), sum(priorities_badges))