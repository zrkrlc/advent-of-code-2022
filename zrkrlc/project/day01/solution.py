from aocd import get_data
import functools as f

def _reducer(a, b, _totals):
    sum = 0
    try:
        sum = int(a) + int(b)

        return sum
    except ValueError:
        _totals.append(a)

        return 0


def solution():
    raw_input = get_data(day=1, year=2022)
    input = raw_input.splitlines()
    
    # Part I
    totals = []
    f.reduce(lambda a, b: _reducer(a, b, totals), input)
    
    # Part II
    top_three = sorted(totals)[-3:]

    return (max(totals), sum(top_three))

        




    
    