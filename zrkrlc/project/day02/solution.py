from aocd import get_data
import functools as f

map_play = {
    'A': { 'X': 1 + 3, 'Y': 2 + 6, 'Z': 3 + 0 },
    'B': { 'X': 1 + 0, 'Y': 2 + 3, 'Z': 3 + 6 },
    'C': { 'X': 1 + 6, 'Y': 2 + 0, 'Z': 3 + 3 }
}

map_play_correct = {
    'A': { 'X': 3 + 0, 'Y': 1 + 3, 'Z': 2 + 6 },
    'B': { 'X': 1 + 0, 'Y': 2 + 3, 'Z': 3 + 6 },
    'C': { 'X': 2 + 0, 'Y': 3 + 3, 'Z': 1 + 6 }
}

def solution():
    raw_input = get_data(day=2, year=2022)
    input = raw_input.splitlines()
    
    # Part I
    rounds = map(lambda s: s.split(), input)
    scores = map(lambda round: map_play[round[0]][round[1]], rounds)
    
    # Part II
    scores_correct = map(lambda round: map_play_correct[round[0]][round[1]], rounds)

    return (sum(scores), sum(scores_correct))
