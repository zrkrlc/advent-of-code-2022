#!/usr/bin/python

import sys
from dotenv import load_dotenv

import day01.solution as day01
import day02.solution as day02
import day03.solution as day03

# Set Advent of Code session token
load_dotenv()

def main(argv):
    """Takes in a number that indicates the solution, corresponding to a particular day, that will be run"""
    solutions = {
        1: day01.solution,
        2: day02.solution,
        3: day03.solution
    }

    result = solutions[int(argv[0])]()
    print(result)
    return result
    

if __name__ == '__main__':
    main(sys.argv[1:])